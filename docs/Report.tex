\documentclass[a4paper]{report}
\usepackage{hyperref}
\usepackage{float}
\usepackage{graphicx}
\graphicspath{ {images/} }

\begin{document}

% Title portion
\title{CS7038 - Group Project  \\A Team Report}


\author{
		Daniel Walsh\\
		{walshd15@tcd.ie}
	\and 
		Manuela Vasconcelos \\
		{vasconcm@tcd.ie}
	\and
		Eugene Kulabuhovs\\
		{kulabuhj@tcd.ie}
}
\maketitle

\section{Background Research and Motivation}
%% Should mention how we wanted to do something educational (language learning etc.)
%% Should also mention the site with the electricity demos
%% DON'T MENTION THE UGLY BABIES!
In our early meetings we were all coming up with wildly different ideas including simple point-and-click games, virtual reality teaching tools, computer vision mobile apps, and e-book translations. It became clear that we needed to find some common ground and so we settled on an educational game. Initially our plan was to make a game to help people learn a new language, since two thirds of the team were bilingual this was an area the group was quite familiar with and we thought could use some improvement. Though we recognised the potential for a language learning application that is more fun than what currently exists, it was difficult to determine an exact approach so we began to consider other kinds of educational applications that may be worth pursuing. 

It was then that we came upon the idea of creating a game teaching the basics of electronics. With every team member having some knowledge of the basic concepts and one even having a degree in electronic engineering, we thought we would be well suited to attempt this task.

The idea came from observing an educational application by Paul Falstad called ``Circuit'' \cite{falstad_circuit}. In the application you are able to draw your own circuits using a simple interface. In the meantime the simulation is running, showing you how current would behave in a real circuit. You can add and remove various analog and digital components and see changes occur in real-time. You can use additional tools such as virtual oscilloscope to analyze your circuit even further. We've noticed that such a level of interactivity immensely helps in the process of learning new concepts in electronic circuit design and thought that we could take it a step further by expanding on the concept and turning it into a game.

Our main motivation was to create a game that manages to teach the basics of electronics and circuit design while also being genuinely enjoyable to play.

\section{Technical Details}
%% Not really sure what to have here apart from saying it's using Unity and stuff.
We implemented our game as a 2D puzzle-platformer built in the Unity game engine. 

\subsection{Circuit Solver}
Though the underlying mathematics of the circuit solver boils down to only a few simple equations, allowing the circuit to be changed in real-time complicates the implementation significantly.

The implemented solution is a recursive algorithm that loops through the circuit and adds all resistances to a running total. If a component is connected in series its resistance is simply added to the total but for a parallel circuit resistances must be processed according to Equation~\ref{eq:parallel_resistance}. 

\begin{equation}
\label{eq:parallel_resistance}
\frac{1}{R} = \frac{1}{R_1} + \frac{1}{R_2} + \frac{1}{R_2} + \dots + \frac{1}{R_n}
\end{equation}

To achieve this, each branch is processed in isolation and the total resistance in that branch is stored in the fist component of the branch as the ``effective resistance'', the resistance in each branch is also added to a running total and stored at the branch point as the ``total resistance in outputs''. 

The solver then skips to the end of the parallel section and continues the summation from there.

Once the resistances have been calculated, the total current is given by Ohm's law ($Voltage=Resistance \times Current$) and the current in each component is calculated according to Equation~\ref{eq:fractional_current}

\begin{equation}
\label{eq:fractional_current}
childCurrent = componentCurrent \times \frac{totalResistanceInOutputs}{branchEffectiveResistance}
\end{equation}

The other special case that must be dealt with is that no current should flow in a broken branch. To handle this, the algorithm will check if the component has no valid outputs (non-null outputs with a path back to the battery) and set the effective resistance of this component to zero. The algorithm will recognise zero effective resistance as invalid and so the change will propagate up the branch so that no current will flow. Using zero as the flag for a break in the circuit does mean that super-conductive components are impossible but that was deemed outside of the scope of this project.

This algorithm runs once every frame, for ease of programming individual components, and to ensure changes are registered in real-time. This represents a large performance bottleneck, especially in more complex circuits so future iterations would likely re-calculate resistances and currents only when parameters change. The algorithm could likely be simplified further by implementing a \href{https://en.wikipedia.org/wiki/Binary_expression_tree}{Binary Expression Tree} and handling branching more implicitly than explicitly.

Individual components may also perform calculations based on their resistance and current, this is most commonly used to calculate the power at that point in the circuit and perform some action if it is above a particular threshold.


\subsection{Electronics Puzzles}


The main goal of the game was to introduce the basics of electric circuits through a simple self-explanatory puzzle-platformer. Our intention was to develop a game that would be accessible to all age groups, but at the same time to be able to introduce more advanced theoretical concepts further on in the game.

Our idea was for the player to manipulate the electric current flowing on the electric circuit placing the electrical components displayed on each level. This way, if right voltage is applied to the components, the player would be able to move through the level and open the exit door to the next level. 

The electrical components for the levels implemented were basically a power source and resistors. The other components as lamps, teleporters, platform switches and doors also have an intrinsic resistance and act as resistor on the circuit. The concept of a simple battery-resistor circuit is introduced on the first level and the complexity of the puzzles increases through the levels.

\subsubsection{Level 1 – Simple Parallel Electric Circuit}

The definition of electric circuit is an interconnection of electrical elements linked together in a closed path so that an electric current may flow continuously \cite{dorf2010}. As it can be seen in Figure~\ref{fig:simple_circuit} the electric components are connected through \textit{wires} and a \textit{charge} may flow in the circuit. 

\begin{figure}[H]
\centering
\includegraphics[width=5cm]{Circuit.png}
\caption{A simple electric circuit.}
\label{fig:simple_circuit}
\end{figure}

The current $i$ is defined as the time rate of change of charge past a given point and is expressed in $ampere (A)$. The voltage existing across an element is defined as the energy required to move the charge from one terminal to another, the unit of voltage is the $volt (v)$.  It is known by Ohm’s law that the current through a conductor between two points is proportional to the voltage across the two points. The constant of proportionality is called $resistance (R)$ and their relationship is described by the Equation ~\ref{eq:ohm}

\begin{equation}
\label{eq:ohm}
v = R \times i
\end{equation}

The resistance of an element or device is the physical property that impedes the flow of current. The power source is a voltage or current generator capable of supplying energy to a circuit.  

This concept is applied on the puzzle of the first level using a parallel circuit. On an electric circuit the places were the elements are connected to each other are called nodes. Figure~\ref{fig:level1} shows the solved parallel circuit of the first level.

\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{level1.png}
\caption{Level editor scene of the first level showing the parallel electric circuit.}
\label{fig:level1}
\end{figure}

The idea of this puzzle was to familiarize the player with how a very basic electric circuit works. After the player has placed the components on the circuit, current flows in the wires. Kirchhoff’s current law states that the algebraic sum of the currents into a node at any instant is zero, and that is visible to the player when a branch of the circuit is closed and the wires light up. When there is enough current flowing through the door, the green bar is filled and it opens bringing the player to the second level.


\subsubsection{Level 2 – Simple Series Electric Circuit}

The idea for the second level is basically the same as for the first level. However, the puzzle is now a series circuit. Figure~\ref{fig:level2} below shows the circuit after the components have been placed.

\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{level2.png}
\caption{Level editor scene of the second  level showing the series electric circuit.}
\label{fig:level2}
\end{figure}

It is visible for the player that there is only one branch in this circuit and the current will only flow after placing all the components in the circuit. The same way, when the current flows, the door opens. With the concepts learned from the first and second levels, the player is able to proceed to the more advanced third and fourth levels.

\subsubsection{Level 3 – Advanced Current Control}

On this level, a more complex circuit is introduced to the player. To reach all the components, on the platforms, the player needs to activate a teleporter. Switches are also introduced in order to give the player the notion of current control. Figure~\ref{fig:level3} shows the circuit of the third level. 

\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{level3.png}
\caption{Level editor scene of the third level – A more complex electric circuit is introduced.}
\label{fig:level3}
\end{figure}

To solve the puzzle, the player needs to understand the circuit of the level is a parallel circuit with two branches. The first branch to be closed is the one which charges the teleporter. The player has to close the switch and then the current flows on the first branch. After activating the teleporter the player is able to reach the higher platforms and to place the components to close the second branch of the circuit. Although current flows on both branches and through the door, this current is being divided on the nodes and the voltage on the door is not providing enough power to open it. That is the tricky part, when the player needs to transform the parallel circuit into a series circuit by opening the leftmost switch, the current flowing through the door is then higher and provides enough power to open it. Now the player can advance to the fourth and last level.

\subsubsection{Level 4 – Current Control of a More Complex Parallel Circuit}

This level has the same concepts as the third level, however it has a bigger circuit comprising four branches in parallel. The idea is to introduce the player to more complex circuits and to revisit all the concepts previously learned. In this case, the flowing current on the first three branches activates a moving platform.  The player needs to find the way through the moving platforms to place the components and to close the switches in order to have the current to flow on the last branch and thus open the door. This can be seen in Figure~\ref{fig:level4}.

\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{level4.png}
\caption{Level editor scene of the fourth level showing the puzzle composed by a parallel circuit with four branches.}
\label{fig:level4}
\end{figure}

The game is completed after the fourth level. The idea can be brought further on with the development of many other levels. The project can be extended by introducing other electric components, such as capacitors, inductors and transistors and concepts like magnetism and boolean logic. 

\subsection{Character Creation and Art of the Game}

The art of the game was mainly done using sprites. A sprite is a two-dimensional bitmap that acts a texture for a 2D object. In our game sprites were used for the background, platforms, resistor component, door, teleporter, switches, nodes, places holders and the main character.  The lamp and wires were done using the built-in \textit{Unity 3D} objects. Apart from the platforms and the main character, the sprites were imported images or simply created on \textit{Microsoft Paint} software. 

\subsubsection{Lampboy}

\textit{Lampboy} was the name chosen for the main character. He was inspired by old popular 2D characters   such as \textit{Mario}, \textit{Mega Man} and \textit{Bomberman}. The character was created from scratch using the vector graphics \textit{Inkscape software}. Inkscape is used by design professionals and hobbyists worldwide, for creating a wide variety of graphics such as illustrations, icons, logos, diagrams, maps and web graphics. \textit{Inkscape} uses the W3G open standard SVG (Scalable Vector Graphics) as its native format, and is free and open-source software \cite{inkscape}. A front-view prototype was first created and later on it was transformed into a side-view character for the development of the animation sprites as it shows in Figure~\ref{fig:lampboy_sprites}. 

\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{lampboySprites.png}
\caption{Lampboy Character created in Inkscape (a) Front-view prototype, (b) Side-view walk loop animation sprites(c) Side-view picking-up item animation sprites.}
\label{fig:lampboy_sprites}
\end{figure}

The platform sprites were also created the same way in \textit{Inkscape}.

\subsection{Animation}
%% Ideas: Software used to create sprite sheets. How animations were added in Unity.

Animation was implemented in Unity by the means of spritesheets. A spritesheet is a hand-crafted bitmap file filled with frame-by-frame animation of the particular character. Animations are done this way because a single bitmap file is a lot easier to manage and can contain tens to hundreds of related animations in the same file. Unity has a simple process of importing spritesheets and then breaking them down back to individual frames that can be used to create animated sprites in the game, shown in Figure~\ref{fig:unity_spritesheet}.

\begin{figure}[H]
\centering
\includegraphics[width=\textwidth]{lampboy.png}
\caption{Representation of a spritesheet in Unity}
\label{fig:unity_spritesheet}
\end{figure}

The spritesheet importer has a simple interface where we can specify the height and the width of each individual frame or we can allow Unity to automatically assign it, as shown in Figure~\ref{fig:sprite_editor}.

\begin{figure}[H]
\centering
\includegraphics{sprite_editor.png}
\caption{Spritesheet importer in action}
\label{fig:sprite_editor}
\end{figure}

\subsection{Platformer}
Platformer elements were implemented using Unity built-in physics simulation engine. Unity makes creation of convincing rigid body behaviours a very straightforward task. A lot of useful parameters such as mass, linear and angular drag, gravity effect can be specified through the Unity UI without writing a single line of code. For our main character all we had to do is specify how controls affect the velocity of the body. 
Colliders were used for all immovable objects such as platforms to prevent the character from falling through them.
Last but not least, the camera control script was added to smoothly follow the character while moving around the level. 

\section{Scrum Process}
%% Probably summarise all the sprints and what we did in them.
This project had four sprints, with a total of 76 story points. 

Daniel acted as the product owner and the development team comprised Daniel, Eugene, and Manuela. Before each sprint we would have a planning meeting to decide on what items in the backlog should be implemented in the forthcoming sprint, and add any new items as needed.
We decided to forgo the daily stand-up meetings as it was difficult to coordinate with our hectic schedules and so Facebook Messenger was used instead to provide regular progress reports to the team. Table~\ref{table:sprint_summary} is a summary of our sprints.

\begin{table}[htbp]
\centering
	\begin{tabular}{|l|c|c|c|c|}
		\hline \bf Sprint & \bf Daniel & \bf Manuela & \bf Eugene & \bf Total\\ \hline
		{Sprint 1} & 5.5 & 3 & 0  & 8.5	\\ \hline
		{Sprint 2} & 8.5 & 0 & 0  & 8.5	\\ \hline
		{Sprint 3} & 9 	 & 6 & 14 & 29	\\ \hline
		{Sprint 4} & 15  & 8 & 7  & 30	\\ \hline
	\end{tabular}
 \caption{Summary of Sprints in the Project}
 \label{table:sprint_summary}
\end{table}

As can be seen from Table~\ref{table:sprint_summary}, there was a significant lack of progress in Sprint 2 because of impending deadlines in other projects. Most of the story points for this sprint were not completed and so they were rolled into the next sprint instead.

\section{Team Responsibilities and Achievements}
%% TODO - Rewrite this 
Each team member contributed items to the product backlog and had input into the final design of the game.

%% Everyone can write their own section describing what they did and what their key achievements were.
\subsection{Daniel}
Daniel was primarily product owner and programmer but also created minor art assets for some of the electrical components. A summary of achievements is given below 

\begin{enumerate}
	\item{Created circuit solver.}
	\item{Implemented scripts and created sprites for the following electronic components.}
	\begin{enumerate}
		\item{switches}
		\item{doors}
		\item{teleporters}
		\item{battery}
	\end{enumerate}
	\item{Bugfixes and Optimisation.}
\end{enumerate}


\subsection{Manuela}

%% Implemented resistor and  light bulb
%% Artwork

Manuela was responsible for the art of the game, creation of the puzzles and level design. She also created two of the electronic components, the resistor and light bulb.

\subsubsection{Art of the Game}

Art of the game means what how the game looks like. As discussed before, it was done by the use of 2D sprites and 3D built-in Unity objects.

\subsubsection{Puzzles and Level Design}

The work was mainly high level and it made use of the game implementation done by Daniel. A theoretical background research in electric circuits was done for the puzzles creation in addition to the platform level design using the assets implemented by Eugene. 

\subsection{Eugene}
%% Platformer character
Eugene was tasked with the implementation of platformer physics and controls for the main character. As well as numerous other small things from adding moving platforms to importing graphics and making sure that components stand out from the background.

% Bibliography
\bibliographystyle{abbrv}
\bibliography{references}
\end{document}