﻿using UnityEngine;
using System.Collections;

public class GlowingObject : MonoBehaviour {

    Light objLight;
    float targetRange = 2;

	// Use this for initialization
	void Start () {
        objLight = gameObject.GetComponent<Light>();
	}
	
	// Update is called once per frame
	void Update () {
        if (objLight.range > 1.9)
            targetRange = 0;

        if (objLight.range < 1.1)
            targetRange = 3;

        objLight.range = Mathf.Lerp(objLight.range, targetRange, 0.5f * Time.deltaTime);
	}
}
