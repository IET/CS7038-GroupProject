﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Circuit : MonoBehaviour
{
	public Battery rootComponent;    //The start of the tree, the power source of the circuit
	public float totalResistance;
	public float totalCurrent = 1.0f;
    private float lastUpdate = 0.0f;
    //Limit calls to update to happen only this many seconds

	// Use this for initialization
	void Start()
	{
        totalResistance = ResolveResistance(rootComponent, null);
        totalCurrent = rootComponent.voltage / totalResistance;
        UpdateCurrents(rootComponent);
    }

    void Update()
    {
        float prevCurrent = totalCurrent;
        totalResistance = ResolveResistance(rootComponent, null);
        totalCurrent = totalResistance > 0.0f ? rootComponent.voltage / totalResistance : 0.0f;


        if (totalCurrent < 0.000001f)
            totalCurrent = 0.0f;

        if (prevCurrent == 0.0f && totalCurrent != 0.0f)
        {
            this.GetComponent<AudioSource>().Play();
        }

        rootComponent.current = totalCurrent;

        ClearCurrents(rootComponent);
        UpdateCurrents(rootComponent);
    }

    void ClearCurrents(ElectricalComponent c)
    {
        if (c.outputs.Count == 0 || c.outputs[0] == null)
            return;

        //If not connected to battery terminal
        if (c.outputs[0].GetComponent<Battery>() != rootComponent)
        {
            foreach (GameObject child in c.outputs)
            {
                if (child == null)
                    continue;

                ElectricalComponent childComponent = child.GetComponent<ElectricalComponent>();
                //Not currently neccessary to reset illumination values at this point but the below code will do that if needed
/*
                foreach (GameObject wire in childComponent.wires)
                {
                    wire.GetComponent<FlexibleWire>().Illuminate(0.0f);
                }
*/
                ClearCurrents(childComponent);
            }
        }

        if (c != rootComponent)
            c.current = 0.0f;
    }

    void UpdateCurrents(ElectricalComponent c)
    {
        if (c.outputs.Count == 0 || c == null)
            return;

        bool connectedToBattery = false;

        //If not connected to battery terminal, calculate current
        for (int i = 0; i < c.outputs.Count; i++)
        {
            if (c.outputs[i] == null)
                continue;

            ElectricalComponent childComponent = c.outputs[i].GetComponent<ElectricalComponent>();
            childComponent.SetCircuit(this);
            float currentOut = 0.0f;

            //This is the link back to the battery
            if (c.outputs[i].GetComponent<Battery>() == rootComponent)
            {
                if (c.wires.Count != 0)
                {
                    if (totalCurrent > 0 && c.current != 0.0f)
                        c.wires[0].GetComponent<FlexibleWire>().Illuminate(1.0f, totalCurrent);
                    else
                        c.wires[0].GetComponent<FlexibleWire>().Illuminate(0.0f, 0.0f);
                }
                connectedToBattery = true;
                continue;
            }

            //If this is a broken branch, set current to 0
            //Also, if another wire coming off of this is connected to the battery 
            //and the above clause wasn't true there can't possibly be current here
            if (childComponent.effectiveResistance == 0.0f || connectedToBattery)
                currentOut = 0.0f;
            else
                currentOut = (c.current * (c.totalResistanceInOutputs / childComponent.effectiveResistance));
            childComponent.current += currentOut;

            if (currentOut > 0.0f)
                c.wires[i].GetComponent<FlexibleWire>().Illuminate((currentOut/totalCurrent), currentOut);
            else if (c.wires.Count >= i)
                c.wires[i].GetComponent<FlexibleWire>().Illuminate(0.0f, 0.0f);

            //If this is a series connection and we're not back at the battery, just keep going
            if ((childComponent.inputs.Count == 1 || c.validBranches == 1) && childComponent != rootComponent)
                UpdateCurrents(childComponent);
        }

        //If this is the last component, return
        if (connectedToBattery)
            return;

        //If this is the start of a branch, continue from the end of the branch
        if (c.validBranches > 1)
        {
            ElectricalComponent nextComponent = FindEndOfBranch(c, c.validBranches);
            if (nextComponent != rootComponent)
                UpdateCurrents(nextComponent);
        }
    }

    /*
    The below function is a complete mess held together with string and paper-clips, I'm really sorry about that.
    If there's time later on I'd like to try to re-do this with a binary expession tree or something
        - Dan
    */

	//Returns total resistnce in cicuit
    //Second parameter is an optional termination point used to define where branches end
    //Can set terminationPoint to null or rootcomponent for series sections
	float ResolveResistance(ElectricalComponent c, ElectricalComponent terminationPoint)
	{
        if (c == null || c.outputs.Count == 0 || (c != rootComponent && c.resistance == 0))
        {
            return 0.0f;
        }

        float runningTotal = 0.0f;
        c.totalResistanceInOutputs = 0.0f;

        //Pre-process the branches to find any breaks
        c.validBranches = 0;
        foreach (GameObject child in c.outputs)
        {
            ElectricalComponent childComponent = child.GetComponent<ElectricalComponent>();
            if (!isBroken(childComponent) && childComponent != null)
                c.validBranches++;
            else
                childComponent.effectiveResistance = 0.0f;
        }

        //series circuit
        if (c.validBranches == 1)
		{
            if (c.outputs[0] == null)
                return 0.0f;

            //We've reached the battery again, return resistance of current component
            if (c.outputs[0].GetComponent<Battery>() == rootComponent)
            {
                c.totalResistanceInOutputs = c.resistance;
                return c.resistance;
             }

            int i = 0;
            //Valid component is not necessarily at [0], iterate until you find it
            ElectricalComponent childComponent = c.outputs[i].GetComponent<ElectricalComponent>();
            while (childComponent == null || isBroken(childComponent))
            {
                childComponent = c.outputs[++i].GetComponent<ElectricalComponent>();
            }

            if (childComponent == terminationPoint)
            {
                c.totalResistanceInOutputs = childComponent.resistance;
                return c.resistance;
            }
            float childResistance = ResolveResistance(childComponent, terminationPoint);

            //If this branch isn't broken, increment total
            if (childResistance > 0)
            {
                runningTotal += childResistance;
                runningTotal += c.resistance;
                c.totalResistanceInOutputs += childComponent.resistance;
            }
            else
            {
                c.totalResistanceInOutputs = 0.0f;
                c.effectiveResistance = 0.0f;
                return 0.0f;
            }
        }
		//parallell circuit
		else if (c.validBranches > 1)
		{
            List<float> childNumerators = new List<float>();
            ElectricalComponent endOfBranch = FindEndOfBranch(c, c.validBranches);

            foreach (GameObject child in c.outputs)
			{
                ElectricalComponent childComponent = child.GetComponent<ElectricalComponent>();
                
                float currChildResistance = ResolveResistance(childComponent, endOfBranch);

                //Stores the total branch resistance in its first component for later current calculations
                childComponent.effectiveResistance = currChildResistance;

                //Inverse and protect against divide by zero errors
                currChildResistance = (currChildResistance > 0) ? (1.0f / currChildResistance) : 0.0f;
                               
                runningTotal += currChildResistance;
                c.totalResistanceInOutputs += currChildResistance;
            }

            //1/R = 1/R1 + 1/R2 .... 1/Rn
            runningTotal = 1.0f / runningTotal;
            c.totalResistanceInOutputs = 1.0f / c.totalResistanceInOutputs;

            //Fast forward to the end of the branch and continue from there
            if (endOfBranch != rootComponent)
            {
                float finalResistance = ResolveResistance(endOfBranch, null);
                if (finalResistance > 0.0f)
                    runningTotal += finalResistance;
                else
                    endOfBranch.totalResistanceInOutputs = 0.0f;
            }

        }
	
		//Total resitance after this node
		return runningTotal;
	}

    //Returns whether the circuit is broken after a certain component
    bool isBroken (ElectricalComponent c)
    {
        //If there's a path to the battery, it's not broken
        if (c == rootComponent)
            return false;
        else if (c.resistance == 0.0f || c.effectiveResistance == 0.0f|| c == null)
            return true;
        else
        {
            for (int i =0; i < c.outputs.Count; i++)
            {
                if (!isBroken(c.outputs[i].GetComponent<ElectricalComponent>()))
                    return false;
            }
            //No path found, it's broken
            return true;
        }
    }

    //Finds the end of a branched section in the circuit
    ElectricalComponent FindEndOfBranch(ElectricalComponent c, int numOutputs)
    {
        if ((c == null) || (c.inputs.Count >= numOutputs))
        {
            return c;
        }

        if (c.outputs.Count == 0 || c.outputs[0] == null)
        {
            //numOutputs--;
            return null;
        }
        if (c.inputs.Count > 1)
            numOutputs -= c.inputs.Count;

        int i = 0;
        ElectricalComponent currComponent = c.outputs[0].GetComponent<ElectricalComponent>();
        currComponent = FindEndOfBranch(currComponent, numOutputs);
        //While the branch isn't valid
        while ((currComponent == null || isBroken(currComponent))
                   && (++i < c.outputs.Count)
                   && currComponent != rootComponent)
        {
            if (c.outputs[i] == null)
            {
                numOutputs--;
                return null;
            }
            currComponent = c.outputs[i].GetComponent<ElectricalComponent>();
            currComponent = FindEndOfBranch(currComponent, numOutputs);
            if (currComponent == rootComponent)
                return currComponent;
        }

        //Return the component at the branch completion point
        return currComponent;
    }
}
