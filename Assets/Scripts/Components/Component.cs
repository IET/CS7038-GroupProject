﻿﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ElectricalComponent : MonoBehaviour {
    //Handle for the parent circuit. Currently needed for rendering
    protected Circuit circuit;

	public float current = 0.0f;
    public float voltage;

	public float resistance = 1.0f;
    public float effectiveResistance;

	// p = i^2 * r
	protected float power;

    public float totalResistanceInOutputs = 0.0f;
    public int validBranches = 0;

	public	List<GameObject> outputs	= new List<GameObject>();
    //Inputs are edited by other scripts but shouldn't be set in the editor
    //[HideInInspector]
    public List<GameObject> inputs	= new List<GameObject>();

    public Rigidbody inputConnection;
    public Rigidbody outputConnection;

    public List<GameObject> wires = new List<GameObject>();

	// Use this for initialization
	void Start () {
        Init();
	}

    protected void Init()
    {
        effectiveResistance = resistance;
        for (int i = 0; i < outputs.Count; i++)
        {
            ElectricalComponent outputComponent;

            if (outputs[i] == null)
            {
                Debug.LogError("Invalid output attached to component: " + this.name);
                continue;
            }
            else
            {
                outputComponent = outputs[i].GetComponent<ElectricalComponent>();

                if (outputComponent == this)
                {
                    Debug.LogError("Component: " + this.gameObject.name + " connected to itself, removing connection before initialising.");
                    outputs.Remove(outputs[i]);
                    i--;
                    continue;
                }

                outputComponent.inputs.Add(this.gameObject);
            }
            //Create connection to output
            if (GameObject.Find("GameManager") == null)
            {
                Debug.LogError("No Game Manager Found in scene.");
                return;
            }
            GameObject wire = Instantiate(GameObject.Find("GameManager").transform.Find("FlexibleWire").gameObject); //TODO - Make this more robust in case that prefab moves
            wire.GetComponent<FlexibleWire>().CreateWire(this.outputConnection, outputComponent.inputConnection);
            //Push wires slightly to the background so that it appears behind components
            wire.transform.position = new Vector3(wire.transform.position.x, wire.transform.position.y, 0.1f);
            wires.Add(wire);
        }
    }

	void FixedUpdate()
	{
        effectiveResistance = resistance;
        validBranches = this.outputs.Count;
	}
    
    public void SetCircuit(Circuit c)
    {
        this.circuit = c;
    }
}
