﻿using UnityEngine;
using System.Collections;

public class Door : ElectricalComponent
{
    public float powerRating = 40;
    public bool isOpen = false;

    private Animator animator;            // Reference to the player's animator component.
    private AnimHealth animHealth;

    public ParticleSystem particleSystem;
    public Light light;

    void Start()
    {
        Init();
        animator = this.GetComponent<Animator>();
        animator.SetBool("isOpen", false);
        animHealth = transform.Find("CurrentBar").GetComponent<AnimHealth>();
    }

    // Update is called once per frame
    void Update () {
        power = current * current * resistance;

        animHealth.TargetHealth = Mathf.Min(1 / powerRating * power, 1);

        if (power >= powerRating)
        {
            if (isOpen == false)
                this.GetComponent<AudioSource>().Play();

            //Open the door
            isOpen = true;
            animator.SetBool("isOpen", true);
            particleSystem.gameObject.SetActive(true);
            light.gameObject.SetActive(true);
        }
        else if (animator.GetBool("isOpen") == true)
        {
            animator.SetBool("isOpen", false);
            particleSystem.gameObject.SetActive(false);
            light.gameObject.SetActive(false);
        }
	}
}
