﻿﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;



public class LightSwitch : ElectricalComponent
{

    static float maxBrightness = 30.0f;

    //Note: Light intensity is in the range 0 - 10
    public new Light light;

    //Default to 40W bulb
    public float powerRating = 40;

    void Start()
    {
        Init();
    }

    // Update is called once per frame
    void Update()
    {
        power = (current * current) * resistance;

        //TODO - Break light at this point
        if (power > powerRating)
        {
            //TODO - Make flash less frequent
            light.enabled = true;// (!light.enabled);
            light.color = new Color(1.0f, 0.0f, 0.0f);
            light.intensity = maxBrightness;

            //Uncomment the below line to have the bulb burn out and break the circuit if broken
            //resistance = 100000000f;
        }
        else
        {
            light.color = new Color(1.0f, 1.0f, 1.0f);
            effectiveResistance = resistance;
        }

        if (power > 0.0f)
        {
            light.enabled = true;
            light.intensity = maxBrightness * (power / powerRating);
        }
        else
        {
            light.enabled = false;
        }
    }
}
