﻿using UnityEngine;
using System.Collections;

public class Teleporter : ElectricalComponent
{
    public float powerRating = 40;
    public bool isOpen = false;

    public ParticleSystem particleSystem;
    public Light light;

    //Use this is you want the teleporter to just work without involving the circuit
    public bool isPowered = false;

    public Teleporter connectedTeleporter;

    private AnimHealth animHealth;

    void Enable()
    {
        isOpen = true;
        light.gameObject.SetActive(true);
        particleSystem.gameObject.SetActive(true);
    }

    void Disable()
    {
        isOpen = false;
        light.gameObject.SetActive(false);
        particleSystem.gameObject.SetActive(false);
    }

    void Start()
    {
        Init();

        animHealth = transform.Find("CurrentBar").GetComponent<AnimHealth>();

        if (isPowered)
            Enable();

        if (connectedTeleporter != null && connectedTeleporter.GetComponent<Teleporter>() == null)
        {
            Debug.LogError("Invalid Teleporter Connected");
        }
        //Just make sure connection goes both ways
        else if (connectedTeleporter != null)
            connectedTeleporter.connectedTeleporter = this;


    }

    // Update is called once per frame
    void Update () {
        if (isPowered)
        {
            animHealth.TargetHealth = Mathf.Min(powerRating, 1);
            Enable();
        }
        else{
            power = current * current * resistance;
            animHealth.TargetHealth = Mathf.Min(1 / powerRating * power, 1);

            if (power >= powerRating)
            {
                if (isOpen == false)
                    Enable();
            }
            else if (isOpen == true)
                Disable();
        }
	}

    public void Teleport(GameObject character)
    {
        if (isOpen)
        {
            if (connectedTeleporter != null && connectedTeleporter.isOpen)
            {
                this.GetComponent<AudioSource>().Play();
                character.transform.position = connectedTeleporter.transform.position;
            }
        }
    }
}
