﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Animator))]
public class Switch : ElectricalComponent
{
    public bool isOpen;
    
    private Animator animator;            // Reference to the player's animator component.

    void Start()
    {
        Init();
        animator = this.GetComponent<Animator>();
        animator.SetBool("isOpen", false);
    }

    // Update is called once per frame
    void Update()
    {
        validBranches = this.outputs.Count;
        if (isOpen)
        {
            //Open the door
            animator.SetBool("isOpen", true);
            effectiveResistance = 0.0f;
        }
        else
        {
            if (animator.GetBool("isOpen") == true)
                animator.SetBool("isOpen", false);

            effectiveResistance = resistance;
        }
    }

    void FixedUpdate()
    { }

    //Flips the switch (changes its state)
    public void Flip()
    {
        this.GetComponent<AudioSource>().Play();
        isOpen = !isOpen;
    }
}
