using UnityEngine;
using System.Collections;

public class MovingPlatformSwitch : ElectricalComponent
{
    public float powerRating = 10;
    public MovingPlatform movingPlatform;
    public SpriteRenderer tip;
    public bool powerOn;
    public float xRange = 0;
    public float yRange = 0;
    // Use this for initialization
    void Start () {
        Init();
    }
	
	// Update is called once per frame
	void Update () {
        power = current * current * resistance;

        if (power >= powerRating && powerOn == false)
            PowerOn();
        else if (power < powerRating && powerOn == true)
            PowerOff();

        tip.color = powerOn ? Color.green : Color.red;

        if (movingPlatform.XRange != (powerOn ? xRange : 0))
        {
            movingPlatform.XRange = powerOn ? xRange : 0;
        }

        if (movingPlatform.YRange != (powerOn ? yRange : 0))
        {
            movingPlatform.YRange = powerOn ? yRange : 0;
        }
    }

    private void PowerOn()
    {
        powerOn = true;
        this.movingPlatform.GetComponent<AudioSource>().Play();
    }

    private void PowerOff()
    {
        powerOn = false;
        this.movingPlatform.GetComponent<AudioSource>().Stop();
    }
}