using System;
using System.Collections;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
using System.Collections.Generic;

namespace UnityStandardAssets._2D
{
    public class PlatformerCharacter2D : MonoBehaviour
    {
        [SerializeField] private float m_MaxSpeed = 10f;                    // The fastest the player can travel in the x axis.
        [SerializeField] private float m_JumpForce = 400f;                  // Amount of force added when the player jumps.
        [Range(0, 1)] [SerializeField] private float m_CrouchSpeed = .36f;  // Amount of maxSpeed applied to crouching movement. 1 = 100%
        [SerializeField] private bool m_AirControl = false;                 // Whether or not a player can steer while jumping;
        [SerializeField] private LayerMask m_WhatIsGround;                  // A mask determining what is ground to the character

        private Transform m_GroundCheck;    // A position marking where to check if the player is grounded.
        const float k_GroundedRadius = .2f; // Radius of the overlap circle to determine if grounded
        private bool m_Grounded;            // Whether or not the player is grounded.
        private Transform m_CeilingCheck;   // A position marking where to check for ceilings
        const float k_CeilingRadius = .01f; // Radius of the overlap circle to determine if the player can stand up
        private Animator m_Anim;            // Reference to the player's animator component.
        private Rigidbody2D m_Rigidbody2D;
        private bool m_FacingRight = true;  // For determining which way the player is currently facing.

        private GameObject holdingItem;
        private GameObject collidingItem = null;

        private AudioSource jumpSound, deathSound, clickSound;

        private void Awake()
        {
            // Setting up references.
            m_GroundCheck = transform.Find("GroundCheck");
            m_CeilingCheck = transform.Find("CeilingCheck");
            m_Anim = GetComponent<Animator>();
            m_Rigidbody2D = GetComponent<Rigidbody2D>();

            //Move to spawn point
            Respawn();

            //Set up audio clips
            AudioSource[] audioSources = this.GetComponentsInChildren<AudioSource>();
            jumpSound = audioSources[0];
            deathSound = audioSources[1];
            clickSound = audioSources[2];
        }

        private void FixedUpdate()
        {
            m_Grounded = false;

            // The player is grounded if a circlecast to the groundcheck position hits anything designated as ground
            // This can be done using layers instead but Sample Assets will not overwrite your project settings.
            Collider2D[] colliders = Physics2D.OverlapCircleAll(m_GroundCheck.position, k_GroundedRadius, m_WhatIsGround);
            for (int i = 0; i < colliders.Length; i++)
            {
                if (colliders[i].gameObject != gameObject)
                    m_Grounded = true;
            }
            m_Anim.SetBool("Ground", m_Grounded);

            // Set the vertical animation
            m_Anim.SetFloat("vSpeed", m_Rigidbody2D.velocity.y);

            if (this.transform.position.y < -15.0f)
            {
                deathSound.Play();
                Respawn();
            }

            if (holdingItem && collidingItem && collidingItem.tag == "Placeholder")
            {
                // Create a pulsing/color changing animation on the placeholder
                collidingItem.GetComponent<SpriteRenderer>().color = Color.Lerp(Color.cyan, Color.red, Mathf.PingPong(Time.time * 4, 1));
                float newScale = Mathf.PingPong(Time.time, 0.5f) + 1;
                collidingItem.transform.localScale = new Vector3(newScale, newScale, newScale);

            }
        }
        
        public void Move(float move, bool crouch, bool jump)
        {
            if ((!m_Grounded || jump) && transform.parent != null)
            {
                GetComponent<Rigidbody2D>().isKinematic = false;
                transform.parent = null;
            }

            // If crouching, check to see if the character can stand up
            if (!crouch && m_Anim.GetBool("Crouch"))
            {
                // If the character has a ceiling preventing them from standing up, keep them crouching
                if (Physics2D.OverlapCircle(m_CeilingCheck.position, k_CeilingRadius, m_WhatIsGround))
                {
                    crouch = true;
                }
            }

            // Set whether or not the character is crouching in the animator
            //m_Anim.SetBool("Crouch", crouch);

            //only control the player if grounded or airControl is turned on
            if (m_Grounded || m_AirControl)
            {
                // Reduce the speed if crouching by the crouchSpeed multiplier
                move = (crouch ? move*m_CrouchSpeed : move);

                // The Speed animator parameter is set to the absolute value of the horizontal input.
                m_Anim.SetFloat("Speed", Mathf.Abs(move));

                // Move the character
                m_Rigidbody2D.velocity = new Vector2(move*m_MaxSpeed, m_Rigidbody2D.velocity.y);

                // If the input is moving the player right and the player is facing left...
                if (move > 0 && !m_FacingRight)
                {
                    // ... flip the player.
                    Flip();
                }
                    // Otherwise if the input is moving the player left and the player is facing right...
                else if (move < 0 && m_FacingRight)
                {
                    // ... flip the player.
                    Flip();
                }
            }
            // If the player should jump...
            if (m_Grounded && jump && m_Anim.GetBool("Ground"))
            {
                // Add a vertical force to the player.
                m_Grounded = false;
                m_Anim.SetBool("Ground", false);
                m_Rigidbody2D.AddForce(new Vector2(0f, m_JumpForce));
                jumpSound.Play();
            }
        }

        private void Respawn()
        {
            this.transform.position = GameObject.Find("LampBoySpawnPoint").transform.position;
        }

        private void Flip()
        {
            // Switch the way the player is labelled as facing.
            m_FacingRight = !m_FacingRight;

            // Multiply the player's x local scale by -1.
            Vector3 theScale = transform.localScale;
            theScale.x *= -1;
            transform.localScale = theScale;
        }

        public void UseDoor()
        {
            if (collidingItem != null && collidingItem.tag == "door" && collidingItem.GetComponent<Door>().isOpen)
            {
                //TODO - Add the following functionality to GameManager
                GameObject.Find("GameManager").GetComponent<GameManager>().LoadNextLevel();
                //Debug.Log("Let me out of here!");
            }
        }

        public void UseTeleporter()
        {
            if (collidingItem != null && collidingItem.tag == "teleporter" && collidingItem.GetComponent<Teleporter>().isOpen)
            {
                collidingItem.GetComponent<Teleporter>().Teleport(this.gameObject);
            }
        }


        public void Action()
        {
            //Nothing to interact with, just return
            if (collidingItem == null)
            {
                if (this.holdingItem)
                {
                    this.holdingItem.transform.parent = null;
                    this.holdingItem.tag = "Collectible";
                    this.holdingItem = null;
                    return;
                }
                else
                    return;
            }

            if (this.holdingItem)
            {
                if (collidingItem.tag == "Placeholder")
                {
                    clickSound.Play();
                    collidingItem.tag = "Untagged";

                    // Leave the item we're holding at the position of the placeholder
                    this.holdingItem.transform.SetParent(collidingItem.transform);
                    this.holdingItem.transform.localPosition = new Vector3(0, 0, 0);
                    this.holdingItem.transform.localScale = new Vector3(1, 1, 1);
                    this.holdingItem.transform.parent = null;
                    collidingItem.GetComponent<Renderer>().enabled = false;

                    StartCoroutine(AutoScale(this.holdingItem.transform.Find("circle"), new Vector3(1000, 1000, 0), 10f));

                    var placeholderComponent = collidingItem.GetComponent<ElectricalComponent>();

                    float holdingDirection = this.holdingItem.GetComponent<ElectricalComponent>().outputConnection.position.x - this.holdingItem.GetComponent<ElectricalComponent>().inputConnection.position.x;
                    float placeholderDirection = placeholderComponent.outputConnection.position.x - placeholderComponent.inputConnection.position.x;

                    if (holdingDirection * placeholderDirection < 0)
                        holdingItem.transform.Rotate(new Vector3(0, 180, 0));

                    // Destroy all the wires that were running FROM our placeholder component
                    foreach (var wire in placeholderComponent.wires)
                    {
                        Destroy(wire);
                    }

                    // Find all the otputs leading to our input and rewire them
                    foreach (var input in placeholderComponent.inputs)
                    {
                        var comp = input.GetComponent<ElectricalComponent>();
                        for (int i = 0; i < comp.outputs.Count; i++)
                        {
                            if (comp.outputs[i] == placeholderComponent.gameObject)
                            {
                                System.Collections.Generic.List<GameObject> wiresToRemove = new System.Collections.Generic.List<GameObject>();

                                FlexibleWire flexiWire = comp.wires[i].GetComponent<FlexibleWire>();
                                // Destroy all the wires that were running TO our placeholder component and remake them to the held component
                                if (flexiWire.endConnection.transform.parent.gameObject == placeholderComponent.gameObject)
                                {
                                    //Leave wires in the same index of the list so they correspond to the outputs
                                    Destroy(comp.wires[i]);
                                    comp.wires[i] = Instantiate(GameObject.Find("GameManager").transform.Find("FlexibleWire").gameObject);
                                    comp.wires[i].GetComponent<FlexibleWire>().CreateWire(comp.outputConnection, holdingItem.GetComponent<ElectricalComponent>().inputConnection);
                                }

                                foreach (var wire in wiresToRemove)
                                {
                                    comp.wires.Remove(wire);
                                    Destroy(wire);
                                }

                                comp.outputs[i] = this.holdingItem;
                                this.holdingItem.GetComponent<ElectricalComponent>().inputs.Add(comp.gameObject);
                            }
                        }
                    }

                    // Rewiring the component
                    var component = this.holdingItem.GetComponent<ElectricalComponent>();
                    component.outputs.AddRange(placeholderComponent.outputs);

                    foreach (var output in component.outputs)
                    {
                        CreateAWireBetween(this.holdingItem.transform, output.transform);
                    }

                    foreach (GameObject output in component.outputs)
                    {
                        ElectricalComponent outputComponent = output.GetComponent<ElectricalComponent>();

                        for (int i = 0; i < outputComponent.inputs.Count; i++)
                        {
                            if (outputComponent.inputs[i] == placeholderComponent.gameObject)
                                outputComponent.inputs[i] = this.holdingItem;
                        }
                    }
                    //Uncomment this line to rip components back out of circuit
                    //this.holdingItem.tag = "Collectible";
                    this.holdingItem = null;
                }

            }
            else
            {
                if (collidingItem.tag == "Collectible")
                {
                    collidingItem.tag = "Untagged";
                    collidingItem.transform.SetParent(this.transform);
                    collidingItem.transform.localPosition = new Vector3(0, 0, 0);
                    this.holdingItem = collidingItem.gameObject;
                    collidingItem = null;
                }
                else if(collidingItem.tag == "switch")
                {
                    //Debug.Log("Flipping Switch");
                    collidingItem.GetComponent<Switch>().Flip();
                }
            }

            if (collidingItem != null && collidingItem.tag == "teleporter")
            {
                UseTeleporter();
            }
        }

        private IEnumerator AutoScale(Transform objectToScale, Vector3 newScale, float time)
        {
            float elapsedTime = 0;
            Vector3 startingScale = objectToScale.localScale;

            while (elapsedTime < time)
            {
                objectToScale.localScale = Vector3.Lerp(startingScale, newScale, (elapsedTime / time));
                elapsedTime += Time.deltaTime;
                yield return new WaitForEndOfFrame();
            }
        }


        void OnCollisionEnter2D(Collision2D coll)
        {
            //Debug.Log("Collision");
        }

        void CreateAWireBetween(Transform startPoint, Transform endPoint)
        {
            var startPointRigid = startPoint.FindChild("output").GetComponent<Rigidbody>();
            var endPointRigid = endPoint.FindChild("input").GetComponent<Rigidbody>();
            GameObject wire = Instantiate(GameObject.Find("GameManager").transform.Find("FlexibleWire").gameObject); //TODO - Make this more robust in case that prefab moves
            wire.GetComponent<FlexibleWire>().CreateWire(startPointRigid, endPointRigid);

            var comp = startPoint.GetComponent<ElectricalComponent>();
            comp.wires.Add(wire);
        }

        void OnTriggerEnter2D(Collider2D other)
        {
            if ((other.tag == "Placeholder" && holdingItem != null) || (other.tag == "Collectible" && holdingItem == null) || other.tag == "switch" || other.tag == "door" || other.tag == "teleporter")
                collidingItem = other.gameObject;

            //Debug.Log("Trigger");
        }

        void OnTriggerExit2D(Collider2D other)
        {
            //Debug.Log("Trigger Exit");
            if (other.tag == "Placeholder" || other.tag == "Collectible" || other.tag == "switch" || other.tag == "door" || other.tag == "teleporter")
                collidingItem = null;

            if (other.tag == "Placeholder")
            {
                other.GetComponent<SpriteRenderer>().color = Color.red;
                other.transform.localScale = new Vector3(1, 1, 1);

            }
        }
    }
}
