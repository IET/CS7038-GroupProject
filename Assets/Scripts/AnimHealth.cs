﻿using UnityEngine;
using System.Collections;

public class AnimHealth : MonoBehaviour {

    public float TargetHealth;

    // Use this for initialization
    void Start () {

    }
	
	// Update is called once per frame
	void Update () {
        var scale = gameObject.transform.localScale;
        scale.x = Mathf.Lerp(scale.x, TargetHealth, 1.5f  * Time.deltaTime);
        gameObject.transform.localScale = scale;
	}
}
