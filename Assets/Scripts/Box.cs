﻿using UnityEngine;
using System.Collections;

public class Box : MonoBehaviour {

    Vector3 initialPosition;

	// Use this for initialization
	void Start () {
        initialPosition = this.transform.position;	
	}
	
	// Update is called once per frame
	void Update () {
        if (this.transform.position.y < -15.0f)
            this.transform.position = initialPosition;
	}

    void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.transform.tag == "Player" || coll.transform.tag == "box")
        {
            //coll.transform.GetComponent<Rigidbody2D>().isKinematic = true;
            coll.transform.parent = transform;
        }
    }
}
