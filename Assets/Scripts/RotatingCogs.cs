﻿using UnityEngine;
using System.Collections;

public class RotatingCogs : MonoBehaviour {

    public MovingPlatform movingPlatform;
    // Use this for initialization
    void Start () {

    }
	
	// Update is called once per frame
	void Update () {
        if (movingPlatform.XRange != 0 || movingPlatform.YRange != 0)
        {
            transform.Rotate(0, 0, 10.0f);
        }
	}
}
