﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    void Awake()
    {
    }

	// Use this for initialization
	void Start () {
	    
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void RestartLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void LoadNextLevel()
    {
        //Loads the next scene by build order. Means we have to keep the build settings making sense
        int currentLevelIndex = SceneManager.GetActiveScene().buildIndex;
        int numScenes = SceneManager.sceneCountInBuildSettings;
        if (currentLevelIndex < numScenes - 1)
            SceneManager.LoadScene(currentLevelIndex + 1);
        else
        {
            //this.GetComponent<AudioSource>().Play();
            Debug.Log("This is the last level, congratulations!");
        }
    }
}
