﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FlexibleWire : MonoBehaviour {

    //The brightest value that each light segment can have
    static float maxLightIntensity = 2.0f;

    public Rigidbody startConnection;
    public Rigidbody endConnection;

    public GameObject wireSegment;

    List<GameObject> segments = new List<GameObject>();

    float lastUpdate = 0.0f;

	// Use this for initialization
	void Start () {
        //segments = new List<GameObject>();     
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void CreateWire(Rigidbody startPoint, Rigidbody endPoint)
    {
        float segmentOverlap = 0.25f;

        this.startConnection = startPoint;
        this.endConnection = endPoint;

        float segmentLength = wireSegment.transform.localScale.y * (1 - segmentOverlap);
        float distance = (endConnection.position - startConnection.position).magnitude;
        int numSegments = (int)(distance / (segmentLength));

        Vector3 wireDirection = (endConnection.position - startConnection.position).normalized;

        segments.Add(Instantiate(wireSegment) as GameObject);
        segments[0].transform.SetParent(this.transform);

        segments[0].transform.position = startConnection.position + (new Vector3(startConnection.transform.localScale.x, 0, 0) / 2) + (wireDirection * (segmentLength));
        //Allow us to update emission color in code
        segments[0].GetComponent<Renderer>().material.EnableKeyword("_EMISSION");

        ConfigurableJoint currJoint = segments[0].GetComponent<ConfigurableJoint>();
        currJoint.connectedBody = startConnection;
        //Allow start connection to freely rotate
        currJoint.angularZMotion = ConfigurableJointMotion.Limited;

        for (int i = 1; i < numSegments; i++)
        {
            segments.Add(Instantiate(wireSegment) as GameObject);

            //Only enable collisions for every 2nd link so that they don't collide with each other
            //segments[segments.Count - 1].GetComponent<BoxCollider>().enabled = (i % 2 == 1);

            //Insert segment into local space
            segments[segments.Count - 1].transform.SetParent(this.transform);

            //Allow us to update emission color in code
            segments[i].GetComponent<Renderer>().material.EnableKeyword("_EMISSION");

            //if (i % 2 == 0)
                //GameObject.Destroy(segments[i].GetComponentInChildren<Light>());

            //Set Position
            segments[segments.Count - 1].transform.position = segments[0].transform.position + (wireDirection * (i * segmentLength));

            //Connect this segment to the previous segment
            currJoint = segments[segments.Count - 1].GetComponent<ConfigurableJoint>();
            currJoint.connectedBody = segments[segments.Count - 2].GetComponent<Rigidbody>();
        }

//        currJoint.angularZMotion = ConfigurableJointMotion.Free;
        //Never enable a collider in the last segment

        currJoint = segments[segments.Count - 1].AddComponent<ConfigurableJoint>();
        currJoint.anchor = new Vector3(0, -1, 0);

        currJoint.autoConfigureConnectedAnchor = false;
        currJoint.connectedAnchor = new Vector3(0.0f, 0.0f, 0.0f);

        //Add motion constraints to end joint
        currJoint.xMotion = ConfigurableJointMotion.Locked;
        currJoint.yMotion = ConfigurableJointMotion.Locked;
        currJoint.zMotion = ConfigurableJointMotion.Locked;

        currJoint.angularXMotion = ConfigurableJointMotion.Locked;
        currJoint.angularYMotion = ConfigurableJointMotion.Locked;
        currJoint.angularZMotion = ConfigurableJointMotion.Free;

        //Copy relevant parameters from prefab
        currJoint.angularZLimit = wireSegment.GetComponent<ConfigurableJoint>().angularZLimit;
        //currJoint.xDrive = wireSegment.GetComponent<ConfigurableJoint>().xDrive;
        //currJoint.yDrive = wireSegment.GetComponent<ConfigurableJoint>().yDrive;
        currJoint.axis = wireSegment.GetComponent<ConfigurableJoint>().axis;
        //Connect to the target end-point
        currJoint.connectedBody = endConnection;
    }

    //Intensity value must be between 0 and 1 to avoid making light too bright
    int j = 1;
    public void Illuminate(float intensity, float current)
    {
        for (int i=0; i < segments.Count; i++)
        {
            if ((Time.time - lastUpdate) > (1/(current * 2.0f)))
            {
                lastUpdate = Time.time;
                j++;
            }

            Light light = segments[i].GetComponentInChildren<Light>();
            if (light == null)
                continue;

            if ((i - j) % 4 == 0)
            {
                light.enabled = intensity > 0.0f;
                light.intensity = intensity * maxLightIntensity;
            }
            else
            {
                light.enabled = false;
            }
        }
    }
}
