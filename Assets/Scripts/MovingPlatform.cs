﻿using UnityEngine;
using System.Collections;

public class MovingPlatform : MonoBehaviour
{
    private float xRange = 0;
    private float deltaTime;

    private float lastXValue = 0;
    public float XRange
    {
        get { return xRange; }
        set
        {
            if (value == 0)
            {
                lastXValue = Time.time - deltaTime;
            }

            xRange = value;
            deltaTime = Time.time - lastXValue;
        }
    }
    private float yRange = 0;
    private float lastYValue = 0;
    public float YRange
    {
        get { return yRange; }
        set
        {
            if (value == 0)
            {
                lastYValue = Time.time - deltaTime;
            }

            yRange = value;
            deltaTime = Time.time - lastYValue;
        }
    }

    Vector3 startPosition;
    float targetX, targetY;

    // Use this for initialization
    void Start()
    {
        startPosition = transform.parent.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (xRange != 0 || yRange != 0)
        {
            transform.parent.position = new Vector3(xRange != 0 ? (Mathf.Sin((Time.time - deltaTime) / xRange * 5 - Mathf.PI / 2) + 1) * xRange / 2 + startPosition.x : startPosition.x,
                yRange != 0 ? (Mathf.Sin((Time.time - deltaTime) / yRange * 5 - Mathf.PI / 2) + 1) * yRange / 2 + startPosition.y : startPosition.y, 0);
        }
    }

    void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.transform.tag == "Player" || coll.transform.tag == "box")
        {
            //coll.transform.GetComponent<Rigidbody2D>().isKinematic = true;
            coll.transform.parent = transform;
        }
    }
}